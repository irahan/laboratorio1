package com.example.laboratorio01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        btnProcesar.setOnClickListener {
            var edad = edtEdad.text.toString().toInt()

            if (edad >=18){
                tvResultado.text= "Usted es mayor de edad"
            }else{
                tvResultado.text="Usted es menor de edad"
            }



        }
    }
}